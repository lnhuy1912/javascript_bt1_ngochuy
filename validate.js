$('#create-button').on({
    'click': function() {
        let isOK = true;
        const groupName = $('#group-name').val();
        const groupType = $('#group-type').val();
        if (groupName === '') {
            $('#group-name').addClass('required');
            isOK = false;
        }
        if (groupType === '') {
            $('#group-type').addClass('required');
            isOK = false;
        }
        if (isOK === false) {
            $(this).attr('disabled', 'disabled');
        }
    },
})
$('#custom-link').on({
    'click': function() {
        $('#choosed-file').click();
    }
});
const resetPreview = function() {
    console.log('sos');
    $('#preview-img').attr('src', '');
    $('#preview-img').css('display', 'none');
    $('#preview-icon').css('display', 'block');
};
$('#choosed-file').on({
    'change': function() {
        const file = this.files[0];
        const typefile = file.type;
        console.log(file.type);
        let typePermisson = ['png', 'jpg', 'jpeg'];
        let isImage = false;
        typePermisson.forEach((value) => {
            if (typefile.endsWith(value)) isImage = true;
        })
        if (isImage) {
            if (file) {
                const reader = new FileReader();
                reader.readAsDataURL(file);
                reader.onload = function() {
                    $('#preview-img').attr('src', this.result);
                    $('#preview-img').css('display', 'block');
                    $('#preview-icon').css('display', 'none');
                }
            }
        } else {
            resetPreview();
        }
    }
})

